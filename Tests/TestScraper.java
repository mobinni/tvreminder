import com.be.mobinni.model.general.Genre;
import com.be.mobinni.model.general.TvShow;
import com.be.mobinni.model.general.TvShowFactory;
import com.be.mobinni.model.webhandlers.IMDBExtractor;
import com.be.mobinni.model.webhandlers.Scraper;
import com.be.mobinni.model.general.SearchType;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.concurrent.ExecutionException;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 26/11/12
 * Time: 23:51
 * To change this template use File | Settings | File Templates.
 */
public class TestScraper {
    public static void main(String[] args) throws InterruptedException, IOException, ExecutionException, ParseException {
         testExtractionIMDB();
      // testURLExtraction();
    }
    @Test
    public static void testExtractionIMDB() throws InterruptedException, IOException, ExecutionException, ParseException {
        System.out.println("\n\nExtract IMDB:");
        System.out.println("------------");
        Scraper s = new Scraper("merlin", SearchType.BYNAME);
        long current = System.currentTimeMillis();
        TvShow show = s.retrieveTvShow();

        //TvShow show  = TvShowFactory.getInstance().getTvShowList().get(0); //get last
        long after = System.currentTimeMillis();


            System.out.println("ID: " + show.getImdb_id());
            System.out.println("Name: " + show.getTitle());
            System.out.println("Year: " + show.getYear());
            System.out.println("Rating: " + show.getRating());
            System.out.println("Creator(s): " + show.getCreators());
            System.out.printf("Genres: ");
            for(Genre g : show.getGenres()){
                System.out.printf("%s, ", g);
            }
            System.out.println("\nNext episode: " + show.getEpisodeNumber());
            System.out.println("Next episode airdate: " + show.getNextEpisode());
        System.out.println("It took: " + (after-current) + " milliseconds to load this show");

    }

    public static void testURLExtraction(){
        System.out.println("\n\nExtact URL");
        System.out.println("------------");
        try {
            URL url = new URL("http://www.imdb.com/find?s=all&q=" + "Stargate+Atlantis");
            String value = IMDBExtractor.extractTvShowIMDBId(url);
            System.out.println("Extracted url fragment: " + value);
        } catch (IOException e) {

        }
    }




}
