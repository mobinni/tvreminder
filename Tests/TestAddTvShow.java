import com.be.mobinni.database.SQLHandler;
import com.be.mobinni.model.general.SearchType;
import com.be.mobinni.model.general.TvShow;
import com.be.mobinni.model.webhandlers.Scraper;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.ExecutionException;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 24/01/13
 * Time: 1:25
 * To change this template use File | Settings | File Templates.
 */
public class TestAddTvShow {
    @Test
    public void addShow() throws IOException, ExecutionException, ParseException, InterruptedException {
        // test add
        Scraper s = new Scraper("the vampire diaries", SearchType.BYNAME);
        TvShow show = s.retrieveTvShow();
        SQLHandler.addTvShow(show);

        // test remove
        SQLHandler.removeTvShow(show);


        // test update
        show.setTitle("te vampier mksdjfmq");
        SQLHandler.addTvShow(show);
        show = s.retrieveTvShow();
        SQLHandler.updateTvShow(show);
    }


}
