package com.be.mobinni.database;

import com.be.mobinni.model.general.Genre;
import com.be.mobinni.model.general.TvShow;
import java.sql.*;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;


/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 19/12/12
 * Time: 1:08
 * To change this template use File | Settings | File Templates.
 */
public class SQLHandler {


    public static Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:hsqldb:file:database/tvdb", "SA", "");
        } catch (SQLException e) {
            System.err.println("Fatal error: cannot create a connection to the database");
            System.exit(1);
        }
        return null;
    }


    public static boolean makeTables() {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            // Autocommit transaction
            connection.setAutoCommit(true);

            statement = connection.prepareStatement("SELECT * FROM   INFORMATION_SCHEMA.SYSTEM_TABLES");
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {         // If no tables exist!
                // Create genre table
                statement = connection.prepareStatement("CREATE TABLE genre ( genre_id INTEGER IDENTITY PRIMARY KEY , genre_name VARCHAR(50) )");
                statement.execute();

                // Create Tvshow table
                statement = connection.prepareStatement("CREATE  TABLE tvshow  ( imdb_id VARCHAR(20) PRIMARY KEY, title VARCHAR(50)," +
                        "                             year INTEGER, season INTEGER, next_episode DATETIME," +
                        "                            rating INTEGER, episode_number INTEGER, number_of_episodes INTEGER)");
                statement.execute();

                // Create table tvshow_genre
                statement = connection.prepareStatement("CREATE TABLE tvshow_genre " +
                        "                    ( imdb_id VARCHAR(20), genre_id INTEGER," +
                        "                      PRIMARY KEY (imdb_id, genre_id)," +
                        "                      FOREIGN KEY(genre_id) REFERENCES genre(genre_id), " +
                        "                      FOREIGN KEY(imdb_id) REFERENCES tvshow(imdb_id) ) ");
                statement.execute();

                // Create creator table
                statement = connection.prepareStatement("CREATE TABLE creator ( creator_id INTEGER IDENTITY PRIMARY KEY, creator_name VARCHAR(70) )");
                statement.execute();


                // Create table tvshow_creator
                statement = connection.prepareStatement("CREATE TABLE tvshow_creator ( imdb_id VARCHAR(20), creator_id INTEGER, " +
                        "                             PRIMARY KEY (imdb_id, creator_id), " +
                        "                             FOREIGN KEY(imdb_id) REFERENCES tvshow(imdb_id), " +
                        "                             FOREIGN KEY(creator_id) REFERENCES creator(creator_id))");
                statement.execute();

                // fill genre table
                statement = connection.prepareStatement("INSERT INTO genre VALUES (?, ?)");
                for (Genre g : Genre.values()) {
                    statement.setInt(1, g.getId());
                    statement.setString(2, g.name());
                    statement.execute();
                }
                statement.close();
            }
            return true;

        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                // do shit

            }
        }
        return false;

    }


    public static boolean removeTvShow(TvShow show) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("DELETE FROM tvshow WHERE imdb_id = ?");
            statement.setString(1, show.getImdb_id());
            statement.execute();

            statement = connection.prepareStatement("DELETE FROM tvshow_genre WHERE imdb_id = ?");
            for (Genre g : show.getGenres()) {
                statement.setString(1, show.getImdb_id());
                statement.execute();
            }

            for (String c : show.getCreators()) {

                PreparedStatement statement1 = connection.prepareStatement("DELETE FROM tvshow_creator WHERE imdb_id = ?");
                statement1.setString(1, show.getImdb_id());
                statement1.execute();
            }
            return true;

        } catch (SQLException e) {
            // do nothing
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                // do nothing
            }
        }
        return false;
}



    public static boolean updateTvShowNextEpisode(TvShow show) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("UPDATE tvshow SET next_episode = ? WHERE imdb_id = ?");
            java.util.Date utilDate = show.getNextEpisode();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            statement.setDate(1, sqlDate);
            statement.setString(2, show.getImdb_id());
            return true;

        } catch (SQLException e) {
            // do nothing
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                // do nothing
            }
        }
        return false;
    }

    public static boolean removeAll(){
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("DROP TABLE tvshow_genre IF EXISTS");
            statement.execute();
            statement = connection.prepareStatement("DROP TABLE tvshow_creator IF EXISTS");
            statement.execute();
            statement = connection.prepareStatement("DROP TABLE creator IF EXISTS");
            statement.execute();
            statement = connection.prepareStatement("DROP TABLE genre IF EXISTS");
            statement.execute();
            statement = connection.prepareStatement("DROP TABLE tvshow IF EXISTS");
            statement.execute();

            makeTables();

            return true;

        } catch (SQLException e) {
            // do nothing
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                // do nothing
            }
        }
        return false;
    }

    public static boolean updateTvShow(TvShow show) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("UPDATE tvshow SET  title = ?, year = ?, season = ?, next_episode = ?," +
                    " rating = ?, episode_number = ?, number_of_episodes = ? WHERE imdb_id = ?");
            statement.setString(1, show.getTitle());
            statement.setInt(2, show.getYear());
            statement.setInt(3, show.getSeason());
            java.util.Date utilDate = show.getNextEpisode();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            statement.setDate(4,  sqlDate);
            statement.setDouble(5, show.getRating());
            statement.setInt(6, show.getEpisodeNumber());
            statement.setInt(7, show.getNumberOfEpisodes());
            statement.setString(8, show.getImdb_id());


            statement.executeUpdate();

            statement = connection.prepareStatement("UPDATE tvshow_genre SET genre_id = ? WHERE imdb_id = ?");
            for (Genre g : show.getGenres()) {
                statement.setInt(1, g.getId());
                statement.setString(2, show.getImdb_id());
                statement.executeUpdate();
            }

            for (String c : show.getCreators()) {
                statement = connection.prepareStatement("SELECT creator_id, creator_name FROM creator WHERE creator_name = ?");
                statement.setString(1, c);
                ResultSet rs = statement.executeQuery();

                PreparedStatement statement1 = connection.prepareStatement("UPDATE tvshow_creator SET creator_id = ? WHERE imdb_id = ?");
                if (rs.next()) {
                    // if creator exists assign value
                    statement1.setInt(1, rs.getInt(1));
                    statement1.setString(2, show.getImdb_id());

                }   else {
                    // else add creator to db & reiterate and get id again
                    PreparedStatement statement2 = connection.prepareStatement("INSERT INTO creator VALUES(null, ?)");
                    statement2.setString(1, c);
                    statement2.execute();
                    statement2.close();

                    statement = connection.prepareStatement("SELECT creator_id, creator_name FROM creator WHERE creator_name = ?");
                    statement.setString(1, c);
                    rs = statement.executeQuery();
                    rs.next();

                    statement1.setInt(1, rs.getInt(1));
                    statement1.setString(2, show.getImdb_id());

                }
                statement1.executeUpdate();
            }
            return true;

        } catch (SQLException e) {
            // do nothing
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                // do nothin
            }
        }
        return false;
    }

    public static boolean retrieveShow(String title) {
        //TODO: retrieve show by title
        return false;
    }

    public static Set<Object> retrieveAll() {
        //TODO: retrieve show by title
        return Collections.emptySet();
    }

    public static boolean addTvShow(TvShow show) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO tvshow VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, show.getImdb_id());
            statement.setString(2, show.getTitle());
            statement.setInt(3, show.getYear());
            statement.setInt(4, show.getSeason());
            java.util.Date utilDate = show.getNextEpisode();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            statement.setDate(5,  sqlDate);
            statement.setDouble(6, show.getRating());
            statement.setInt(7, show.getEpisodeNumber());
            statement.setInt(8, show.getNumberOfEpisodes());

            statement.execute();

            statement = connection.prepareStatement("INSERT INTO tvshow_genre VALUES(?, ?)");
            for (Genre g : show.getGenres()) {
                statement.setString(1, show.getImdb_id());
                statement.setInt(2, g.getId());
                statement.execute();
            }

            for (String c : show.getCreators()) {
                statement = connection.prepareStatement("SELECT creator_id, creator_name FROM creator WHERE creator_name = ?");
                statement.setString(1, c);
                ResultSet rs = statement.executeQuery();

                PreparedStatement statement1 = connection.prepareStatement("INSERT INTO tvshow_creator VALUES(?, ?)");
                if (rs.next()) {
                    // if creator exists assign value
                    statement1.setString(1, show.getImdb_id());
                    statement1.setInt(2, rs.getInt(1));
                }   else {
                    // else add creator to db & reiterate and get id again
                    PreparedStatement statement2 = connection.prepareStatement("INSERT INTO creator VALUES(null, ?)");
                    statement2.setString(1, c);
                    statement2.execute();
                    statement2.close();

                    statement = connection.prepareStatement("SELECT creator_id, creator_name FROM creator WHERE creator_name = ?");
                    statement.setString(1, c);
                    rs = statement.executeQuery();
                    rs.next();

                    statement1.setString(1, show.getImdb_id());
                    statement1.setInt(2, rs.getInt(1));
                }
                statement1.execute();
            }
            return true;

        } catch (SQLException e) {
            // do nothing
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                // do nothin
            }
        }
        return false;
    }
}




