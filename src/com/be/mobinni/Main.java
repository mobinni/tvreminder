package com.be.mobinni;



import com.be.mobinni.database.SQLHandler;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.FileHandler;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 26/11/12
 * Time: 23:04
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) throws SQLException {

        try {
            // connect to db
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            System.err.println("Fatal error: cannot load"
                    + " database driver");
            System.exit(1);
        }
        // make tables if they don't exist
        SQLHandler.makeTables();



    }
}
