package com.be.mobinni.model.webhandlers;

import com.be.mobinni.model.other.DateUtils;
import com.be.mobinni.model.general.Genre;
import com.be.mobinni.model.general.TvShow;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;


/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 27/11/12
 * Time: 2:01
 * To change this template use File | Settings | File Templates.
 */
public final class IMDBExtractor implements Callable<TvShow> {
    //index of the html DOM object
    private int episodeIndex = 0;
    //boolean to check if there's a next apisode
    private boolean nextEpisode;
    private URL url;

    public IMDBExtractor(URL url) {
        this.url = url;
    }

    public TvShow extractTvShow() throws IOException, ParseException {
        TvShow show;
        Document doc = Jsoup.connect(url.toString()).userAgent("Mozilla").get();

        //Set show attributes
        String id = url.toString().substring(url.toString().indexOf("/tt") + 1, url.toString().length());

        show = new TvShow();
        show.setImdb_id(id.replaceAll("/", ""));
        show.setTitle(extractSeriesName(doc));
        show.setRating(extractRating(doc));
        show.setCreators(extractCreators(doc));
        show.setYear(extractYear(doc));
        show.setNextEpisode(extractNextEpisode(doc, url));
        show.setEpisodeNumber(extractEpisodeNumber(doc, url));
        show.setNumberOfEpisodes(extractNumberOfEpisodes(doc, url));
        show.setGenres(extractGenres(doc));
        show.setSeason(getSeasonNumber(doc));

        return show;
    }

    private Document getLatestSeasonDoc(Document doc, URL urlShow) throws IOException {
        String season = doc.body().getElementsByAttributeValueContaining("href", "episodes?").get(0).text();
        URL url = new URL(urlShow + "episodes?season=" + season);

        //read document
        return Jsoup.connect(url.toString()).userAgent("Mozilla").get();

    }

    private int getSeasonNumber(Document doc) {
        String season = doc.body().getElementsByAttributeValueContaining("href", "episodes?").get(0).text();
        return Integer.parseInt(season);

    }

    private String extractSeriesName(Document doc) throws IOException {
        String name = doc.body().getElementsByTag("h1").get(0).text();
        name = name.substring(0, name.indexOf('('));
        return name;
    }

    private int extractYear(Document doc) {
        String year = doc.body().getElementsByTag("h1").get(0).text();

        int finalIndex = (year.contains("–")) ? year.indexOf("–") : year.indexOf(")");
        year = year.substring(year.indexOf('(') + 1, finalIndex);
        return Integer.parseInt(year);
    }

    private Double extractRating(Document doc) throws IOException {
        String rating = doc.body().getElementById("overview-top").getElementsByTag("div").get(3).text();
        return Double.parseDouble(rating);
    }

    private Date extractNextEpisode(Document doc, URL urlShow) throws IOException, ParseException {
        Document seasonDoc = getLatestSeasonDoc(doc, urlShow);
        int size = seasonDoc.getElementsByAttributeValue("class", "airdate").size();
        for (int i = 0; i < size; i++) {
            String airdate = seasonDoc.getElementsByAttributeValue("class", "airdate").get(i).text();
            if (airdate.length() > 11) {
                if (!airdate.toLowerCase().equalsIgnoreCase("unknown")) {
                    //check when the next episode is
                    nextEpisode = DateUtils.nextEpisodeCheck(DateUtils.convertDateIMDB(airdate));
                    if (nextEpisode) {
                        episodeIndex = i;
                        return DateUtils.convertDateIMDB(airdate);
                    } else {
                        episodeIndex = 0;
                    }
                }
            }
        }
        // return dummy date
        return new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900");

    }

    private int extractEpisodeNumber(Document doc, URL urlShow) throws IOException {
        if (nextEpisode) {
            Document seasonDoc = getLatestSeasonDoc(doc, urlShow);
            String number = seasonDoc.body().getElementsByAttribute("content").get(episodeIndex + 2).toString();
            number = number.substring(number.indexOf("content=\"") + 9, number.indexOf("\" />"));
            return Integer.parseInt(number);// Integer.parseInt(number);
        }
        return 0;

    }

    private int extractNumberOfEpisodes(Document doc, URL urlShow) throws IOException {
            Document seasonDoc = getLatestSeasonDoc(doc, urlShow);
            int index =   seasonDoc.body().getElementsByAttribute("content").size();
            String number = seasonDoc.body().getElementsByAttribute("content").get(index - 1).toString();
            number = number.substring(number.indexOf("content=\"") + 9, number.indexOf("\" />"));
            return Integer.parseInt(number);// Integer.parseInt(number);
    }

    private HashSet<Genre> extractGenres(Document doc) throws IOException {
        HashSet<Genre> genres = new HashSet<>();
        int size = doc.body().getElementsByAttributeValueContaining("href", "/genre/").size() / 2;
        for (int i = 0; i < size; i++) {
            String genre = doc.body().getElementsByAttributeValueContaining("href", "/genre/").get(i).text();
            genres.add(Genre.valueOf(genre.replaceAll("-", "")));
        }
        return genres;
    }

    private List<String> extractCreators(Document doc) {
        Elements e = doc.body().getElementsByAttributeValue("class", "txt-block");
        String creator = "";
        String [] temp = {"no creator(s)"};
        List<String> creators = new ArrayList<>();
        for (int i = 0; i < e.size(); i++) {
            creator = e.get(i).toString();
            if (creator.toLowerCase().contains("creator")) {
                creator = e.get(i).text();
                temp = creator.substring(creator.indexOf(":") + 1, creator.length()).split(",");
            }
        }
        for(String s : temp) {
            creators.add(s);
        }

        return creators;
    }

    public static String extractTvShowIMDBId(URL url) throws IOException {
        Document doc = Jsoup.connect(url.toString()).userAgent("Mozilla").get();
        // Imdb can redirect if there is only one result
        if (doc.title().equalsIgnoreCase("Find - IMDb")) {
            Elements e = doc.body().getElementsByAttributeValue("class", "result_text");
            for (int i = 0; i < e.size(); i++) {
                String id = e.get(i).toString();
                if (id.toLowerCase().contains("(tv series)")) {
                    return id.substring(id.indexOf("/title/") + 7, id.indexOf("/?ref"));
                }
            }
        } else {
            return doc.baseUri().substring(doc.baseUri().indexOf("/title/") + 7, doc.baseUri().length());
        }
        return null;
    }

    @Override
    public TvShow call() throws Exception {
        return extractTvShow();  //To change body of implemented methods use File | Settings | File Templates.
    }

}
