package com.be.mobinni.model.webhandlers;



import com.be.mobinni.model.general.SearchType;
import com.be.mobinni.model.general.TvShow;
import com.be.mobinni.model.general.TvShowFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 26/11/12
 * Time: 23:17
 * To change this template use File | Settings | File Templates.
 */
public class Scraper {
    private String value;
    private SearchType type;

    public Scraper (String value, SearchType type){
        this.value = value;
        this.type = type;

    }

    public TvShow retrieveTvShow() throws IOException, ParseException, InterruptedException, ExecutionException {
        URL url;
        // Retrieve data from imdb api website in XML format
        if(type.equals(SearchType.BYNAME)){
            value = value.replaceAll(" ", "+");
            url = new URL("http://www.imdb.com/find?s=all&q=" + value );
            value = IMDBExtractor.extractTvShowIMDBId(url);
        }
        url = new URL("http://www.imdb.com/title/" + value);

        // call executor add thread to pool => wait for exec
        TvShow show;
        IMDBExtractor extractor = new IMDBExtractor(url);
        ExecutorService executor = Executors.newFixedThreadPool(10);

        Future<TvShow> submit = executor.submit(extractor);
        show = submit.get();
        executor.shutdown();

        return show;
    }




}

