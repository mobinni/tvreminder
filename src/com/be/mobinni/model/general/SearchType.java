package com.be.mobinni.model.general;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 27/11/12
 * Time: 1:07
 * To change this template use File | Settings | File Templates.
 */
public enum SearchType {
    BYNAME("by name"),
    BYID("by id");

    private String name;

    private SearchType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
