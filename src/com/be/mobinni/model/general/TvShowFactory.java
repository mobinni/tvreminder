package com.be.mobinni.model.general;

import com.be.mobinni.database.SQLHandler;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 26/11/12
 * Time: 23:17
 * To change this template use File | Settings | File Templates.
 */
public class TvShowFactory {
    private static TreeSet<TvShow> tvShowList = new TreeSet<>();
    private static TvShowFactory instance = new TvShowFactory();

    private TvShowFactory(){
         //no code required - singleton
    }

    public void addTvShow(TvShow show){
        if(!tvShowList.contains(show)){
            tvShowList.add(show);
            SQLHandler.addTvShow(show);
        } else {
          throw new IllegalArgumentException("Show is already in list");
        }
    }

    public void removeTvShow(TvShow show) {
        if(tvShowList.contains(show)) {
            tvShowList.remove(show);
            SQLHandler.removeTvShow(show);
        }
    }

    public void updateShow(TvShow show) {
        if(tvShowList.contains(show)) {
            Iterator it = tvShowList.iterator();
            while (it.hasNext()) {
                TvShow current = (TvShow) it.next();
                if(current.equals(show)){
                    it.remove();
                    tvShowList.add(show);
                }
            }
            SQLHandler.updateTvShow(show);
        }
    }

    public TreeSet<TvShow> getTvShowList(){
        return tvShowList;
    }

    public static TvShowFactory getInstance(){
        return instance;
    }
}
