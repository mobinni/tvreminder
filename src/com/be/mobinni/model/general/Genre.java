package com.be.mobinni.model.general;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 26/11/12
 * Time: 23:06
 * To change this template use File | Settings | File Templates.
 */
public enum Genre {


    Action(1),
    Adventure(2),
    Animation(3),
    Biography(4),
    Comedy(5),
    Crime(6),
    Documentary(7),
    Drama(8),
    Family(9),
    Fantasy(10),
    Filmnoir(11),
    Gameshow(12),
    History(13),
    Horror(14),
    Music(15),
    Musical(16),
    Mystery(17),
    News(18),
    RealityTV(19),
    Romance(20),
    SciFi(21),
    Sport(22),
    TalkShow(23),
    Thriller(24),
    War(25),
    Western(26);

    private int id;

    private Genre(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
