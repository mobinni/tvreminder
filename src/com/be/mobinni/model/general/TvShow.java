package com.be.mobinni.model.general;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 26/11/12
 * Time: 23:04
 * To change this template use File | Settings | File Templates.
 */
public class TvShow  implements Comparable<TvShow>{
    private String title;
    private HashSet<Genre> genres;
    private List<String> creators;
    private int year;
    private Date nextEpisode;
    private double rating;
    private String imdb_id;
    private int episodeNumber;
    private int numberOfEpisodes;
    private int season;


    public TvShow(String imdb_id, String title, double rating, List<String> creators, int year, Date nextEpisode, int episodeNumber,
                  int numberOfEpisodes,  HashSet<Genre> genres, int season) {
        this.title = title;
        this.genres = genres;
        this.creators = creators;
        this.year = year;
        this.nextEpisode = nextEpisode;
        this.rating = rating;
        this.episodeNumber = episodeNumber;
        this.numberOfEpisodes = numberOfEpisodes;
        this.imdb_id = imdb_id;
        this.season = season;
    }

    public TvShow() {
        genres = new HashSet<>();
        creators = new ArrayList<>();
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenres(HashSet<Genre> genres) {
        this.genres = genres;
    }

    public void setCreators(List<String> creators) {
        this.creators = creators;
    }


    public void setYear(int episodesCount) {
        this.year = episodesCount;
    }

    public void setNextEpisode(Date latestEpisode) {
        this.nextEpisode = latestEpisode;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public void setNumberOfEpisodes(int numberOfEpisodes) {
        this.numberOfEpisodes = numberOfEpisodes;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public int getNumberOfEpisodes() {
        return numberOfEpisodes;
    }

    public String getTitle() {
        return title;
    }

    public HashSet<Genre> getGenres() {
        return genres;
    }

    public List<String> getCreators() {
        return creators;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TvShow tvShow = (TvShow) o;

        if (episodeNumber != tvShow.episodeNumber) return false;
        if (numberOfEpisodes != tvShow.numberOfEpisodes) return false;
        if (Double.compare(tvShow.rating, rating) != 0) return false;
        if (season != tvShow.season) return false;
        if (year != tvShow.year) return false;
        if (creators != null ? !creators.equals(tvShow.creators) : tvShow.creators != null) return false;
        if (genres != null ? !genres.equals(tvShow.genres) : tvShow.genres != null) return false;
        if (imdb_id != null ? !imdb_id.equals(tvShow.imdb_id) : tvShow.imdb_id != null) return false;
        if (nextEpisode != null ? !nextEpisode.equals(tvShow.nextEpisode) : tvShow.nextEpisode != null) return false;
        if (title != null ? !title.equals(tvShow.title) : tvShow.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = title != null ? title.hashCode() : 0;
        result = 31 * result + (genres != null ? genres.hashCode() : 0);
        result = 31 * result + (creators != null ? creators.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (nextEpisode != null ? nextEpisode.hashCode() : 0);
        temp = rating != +0.0d ? Double.doubleToLongBits(rating) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (imdb_id != null ? imdb_id.hashCode() : 0);
        result = 31 * result + episodeNumber;
        result = 31 * result + numberOfEpisodes;
        result = 31 * result + season;
        return result;
    }

    public int getYear() {
        return year;
    }

    public Date getNextEpisode() {
        return nextEpisode;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public int compareTo(TvShow o) {
        int result = this.title.compareTo(o.title);
       if(result == 0){
           return (int)(this.rating - o.rating);
       }
        return result;
    }
}
