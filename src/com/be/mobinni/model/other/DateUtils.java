package com.be.mobinni.model.other;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 28/11/12
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */
public class DateUtils {


    public static Date convertDateIMDB(String date) {
        try {
            int month = getMonth(date.substring(0, 3));
            String day;

            if (date.substring(0, 3).equalsIgnoreCase("may")) {
                day = date.substring(date.indexOf('y') + 2, date.indexOf(','));
            } else {
                day = date.substring(date.indexOf('.') + 2, date.indexOf(','));
            }
            String year = date.substring(date.indexOf(',') + 2, date.length());

            Date airdate = new SimpleDateFormat("dd/MM/yyyy").parse(day + "/" + month + "/" + year);

            return airdate;
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public static boolean nextEpisodeCheck(Date air) throws ParseException {
        DateTime currDT = new DateTime(new Date());
        DateTime airDT = new DateTime(air);
        int days = Days.daysBetween(currDT, airDT).getDays();

            return (days <= 7 && days > 0);

    }

    private static int getMonth(String month) {
        switch (month) {
            case "Jan":
                return 1;
            case "Feb":
                return 2;
            case "Mar":
                return 3;
            case "Apr":
                return 4;
            case "May":
                return 5;
            case "Jun":
                return 6;
            case "Jul":
                return 7;
            case "Aug":
                return 8;
            case "Sep":
                return 9;
            case "Oct":
                return 10;
            case "Nov":
                return 11;
            case "Dec":
                return 12;
            default:
                return 0;
        }
    }
}
